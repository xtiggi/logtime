﻿using System;

namespace ConsoleCore.Domains.Logs
{
    public class Log
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public TimeSpan Time { get; set; }
        public DateTime TimeLog { get; set; }
    }
}