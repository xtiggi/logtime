﻿using ConsoleCore.Data;
using System.Linq;

namespace ConsoleCore.Services
{
    public interface IBaseServiceRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);

        /// <summary>
        /// Delete entity
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);

        /// <summary>
        /// Insert entity
        /// </summary>
        /// <param name="entity"></param>
        void Insert(T entity);

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);

        /// <summary>
        /// Get IQuearyable<T>
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetQuery();
    }
}