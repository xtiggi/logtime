﻿using ConsoleCore.Data;

namespace ConsoleCore.Services.Logs
{
    public interface ILogService : IBaseServiceRepository<Log>
    {
    }
}
