﻿using ConsoleCore.Data;
using System;
using System.Collections.Generic;

namespace ConsoleCore.Services.Logs
{
    public interface ILogRepository : IBaseServiceRepository<Log>
    {
        List<Log> GetByStartDateTime(DateTime startDateTime);
    }
}