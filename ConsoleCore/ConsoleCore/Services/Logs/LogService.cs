﻿using ConsoleCore.Data;
using ConsoleCore.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleCore.Services.Logs
{
    public class LogService : BaseRepository<Log>, ILogRepository
    {
        public List<Log> GetByStartDateTime(DateTime startDateTime)
        {
            return GetQuery().Where(x => x.TimeLog > startDateTime).ToList();
        }
    }
}