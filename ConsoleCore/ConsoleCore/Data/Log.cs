﻿using System;

namespace ConsoleCore.Data
{
    public class Log : BaseEntity
    {
        public string Description { get; set; }
        public TimeSpan Time { get; set; }
        public DateTime TimeLog { get; set; }
    }
}